import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AmazonTest {

    private WebDriver driver;


    @Before
    public void abrir (){
        System.setProperty("webdriver.gecko.driver", "C:/ageckodriver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br/");
    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void maisVendidos() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
    }

    @Test
    public void testarBuscador() throws InterruptedException{
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
     driver.findElement(By.id("nav-link-accountList-nav-line-1")).click();
     driver.findElement(By.id("ap_email")).sendKeys("jsjsjs@gmail.com");
     driver.findElement(By.id("continue")).click();
     Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
    }

    @Test
    public void BuscarLivro() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[5]")).click();
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Os Miseraveis");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
    }
    @Test
    public void NovidadesnaAmazon() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(2000);
        Assert.assertEquals("Página 1 de 5", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/div/div/div/div/div[1]/div[2]/span/span[1]")).getText());

    }

    @Test
    public void TestarOfertasRelampagos () throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[3]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[1]/div[16]/div/div/div/div/div[1]/div/div/div/div[2]/div/ol/li[2]/a/span[1]/img")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Ofertas e Promoções", driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div/div/h1")).getText());
    }

    @Test
    public void ProdutosemAlta() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[4]/a")).click();
        Thread.sleep(2000);
        Assert.assertEquals("Página 1 de 5", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/div/div/div/div/div[1]/div[2]/span/span[1]")).getText());

    }

    @Test
    public void VerCarrinho () throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("1984");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[4]/div/div/div/div/div/div/div[2]/div[1]/h2/a/span")).click();
        driver.findElement(By.id("add-to-cart-button")).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[1]/div[2]/div/div[1]/span/span/a")).click();
        Assert.assertEquals("Carrinho de compras", driver.findElement(By.xpath("/html/body/div[1]/div[4]/div/div[3]/div/div[2]/div[1]/div/div/div/h1")).getText());
    }
}

